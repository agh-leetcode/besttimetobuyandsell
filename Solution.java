class Solution {

    public static void main(String[] args) {
        Solution solution = new Solution();
        int prices [] = new int[]{7,1,5,3,6,4};
        System.out.println(solution.maxProfit(prices));
    }

    /*private int min = Integer.MAX_VALUE;
    public int maxProfit(int[] prices) {
        return Arrays.stream(prices).map(i -> i - (min = Math.min(min, i))).max().orElse(0);
    }*/

    //better solution
    public int maxProfit(int[] prices) {
        int minPrice = Integer.MAX_VALUE;
        int maxProfit = 0;

        for(int i=0;i<prices.length;i++){
            if(prices[i] < minPrice){
                minPrice = prices[i];
            }else if(prices[i] - minPrice > maxProfit){
                maxProfit = prices[i] - minPrice;
            }
        }
        return maxProfit;
    }
}